import mpd

from mpd import MPDClient
from time import sleep

class mpd_client:
    def __init__(self, ip='localhost', port=6600):
        self.ip = ip
        self.port = port
        self.client = MPDClient()
        self.client.timeout = 10
        self.client.idletimeout = None

    def connect(self):
        self.client.connect(self.ip, self.port)

    def close(self):
        self.client.close()                     # send the close command
        self.client.disconnect()


    def get_status(self):
        try:
            status = self.client.status()
        except mpd.ConnectionError as e:
            if 'Not connected' == e.message:
                self.connect()
                status = self.client.status()
            else:
                raise NameError('Reconnect not possible')
        return status

    def set_vol_rel(self, delta_vol):
        status = self.get_status()
        vol= float(status['volume'])
        new_vol = delta_vol+vol

        if new_vol>100:
            new_vol=100
        if new_vol<0:
            new_vol=0
        print(new_vol)
        self.client.setvol(int(round(new_vol)))






if __name__  == '__main__':
    mpc = mpd_client()
    while True:
        for i in range(100):
            mpc.set_vol_rel(1)
            sleep(0.1)
        for i in range(100):
            mpc.set_vol_rel(-1)
            sleep(0.1)

    mpc.close()
