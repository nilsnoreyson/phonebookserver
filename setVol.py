import subprocess

def setVol(value):
    if value>=0:
        value = '+{}'.format(value)
    else:
        value = '-{}'.format(value)
        
    subprocess.call(['mpc', 'vol',value], shell=False)

setVol(3)
