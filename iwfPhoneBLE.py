#!/root/PhoneBookProject/venv/bin/python
# -*- coding: utf-8 -*-

__author__ = 'peterb'

#import serial
import datetime
#from mpd import MPDClient
#import urllib2
#from ble_phone import BTLE_PHONE
#import mpc_client.mpc_client as MPC
import time
import os
import sys
import zmq
import pickle


if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
else:
    import subprocess


def pause():
    print('Toggle')
    subprocess.call(['mpc', 'toggle'], shell=False)
#    subprocess.call(['mpc', 'pause'], shell=False)
    get_playlist_num_dict()
def next():
    subprocess.call(['mpc', 'next'], shell=False)

def prev():
    subprocess.call(['mpc', 'prev'], shell=False)

import re

def get_playlist_num_dict():
    out = subprocess.check_output(["mpc", "lsplaylists"])
    playlists = out.decode().split('\n')
    number_dict = {}
    for p in playlists:
        number_in_names = re.findall( "^.*#(\d*)", p)
        if len(number_in_names)>0:
            for number_in_name in number_in_names:
                if number_in_name in number_dict:
                    number_dict[number_in_name].append(p)
                else:
                    number_dict[number_in_name] = [p]
    print(number_dict)
    return number_dict 

def play_by_number(number):
    number = str(number)
    print(f"Dailed number: {number}")
    numbers_dict = get_playlist_num_dict()
    if number in numbers_dict:
        subprocess.call(['mpc', 'clear'], shell=False)
        playlist_names = numbers_dict[number]
        print('playlist_names')
        print('Play playlists:')
        for playlist_name in playlist_names:
            print(f'    {playlist_name}')
            subprocess.call(['mpc', 'load',playlist_name], shell=False)
    if len(playlist_names)>1:
        subprocess.call(['mpc', 'shuffle'], shell=False)
        print('SHUFFLE TIME!')
    subprocess.call(['mpc', 'play'], shell=False)
    print(f'Rock and roll #{number}!')


# def setVol(value):
#
#     mpc.set_vol_rel(value)

def setVol(value):
    if value>=0:
        value = '+{}'.format(value)
    else:
        value = '{}'.format(value)

    subprocess.call(['mpc', 'vol',value], shell=False)

#mpc = MPC.mpd_client()


play_by_number("6")


context    = zmq.Context()
subscriber = context.socket(zmq.SUB)

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)
    print(port)


subscriber.connect("tcp://localhost:{}".format(port))

if len(sys.argv) > 2:
    port1 =  sys.argv[2]
    int(port1)
    print(port1)
    subscriber.connect ("tcp://localhost:%s" % port1)

# Subscribe to zipcode, default is NYC, 10001
topicfilter = b"Phone"
subscriber.setsockopt(zmq.SUBSCRIBE, topicfilter)

poller = zmq.Poller()
poller.register(subscriber, zmq.POLLIN)

mpdAddress = 'localhost'
mpdPort = 6600



connectTime=datetime.datetime.now()


actionTime=False
lastAction=datetime.datetime.now()
actionTimestamp=0


LOST_ACTION_TIME=10


last_number_time=datetime.datetime.now()
dail_timeout=3
number=0
newDail=False

vals_old = []
vals = None
while True:
    if (datetime.datetime.now()-lastAction).total_seconds()>LOST_ACTION_TIME:
        actionTime=False

    socks = dict(poller.poll(10))

    if socks.get(subscriber) == zmq.POLLIN:
        print('\n')
        [topic,msg] = subscriber.recv_multipart()
        messagedata = pickle.loads(msg)
        print(messagedata)
        if messagedata:
            vals = messagedata
        else:
            vals = None       
    else:
        vals = None
        #vals = ble.get_last_command()

    if vals and len(vals)==3:
        print('vals',vals)
        if vals[0]==2:
            actionTime=True
            lastAction=datetime.datetime.now()

            if vals[1]==1 or vals[1]==0:
                if vals[1]==1:
                    changeVol=+5
                elif vals[1]==0:
                    changeVol=-7
                try:
                    setVol(changeVol)
                except:
                    print('setting volume failed')
        if vals[0]==1:
            if vals[1]==0:
                try:
                    pause()
                except:
                    print('pause failed')
            elif vals[1]==1:
                try:
                    prev()
                except:
                    print('backseek failed')
            elif vals[1]==2:
                try:
                    next()
                except:
                    print('next failed')



        if vals[0]==3:
            new_number=vals[1]
            new_number=int(new_number)
            last_number_time=datetime.datetime.now()
            number=number*10+new_number
            #print(number)
            newDail=True
    else:
        #time.sleep(0.1)
        #print('no signal')
        pass

    if newDail and ((datetime.datetime.now()-last_number_time).total_seconds()>dail_timeout):
        #print('play number%s'%number)
        try:
            play_by_number(number)
        except:
            print('play number error')
        number=0
        newDail=False





